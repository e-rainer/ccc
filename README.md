tdb

Kinect startup
sudo modprobe -r gspca_kinect

docker build -t rviz -f Dockerfile-rviz.yaml .
docker run -it --net host -v /tmp/.X11-unix:/tmp/.X11-unix -e DISPLAY=unix$DISPLAY rviz

http://wiki.ros.org/openni_launch/Tutorials/QuickStart
rosrun image_view image_view image:=/camera/rgb/image_color
rosrun image_view image_view image:=/camera/depth/image
