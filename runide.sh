docker run -d -p 8080:8080 \
  -v "/home/erainer/docker:/home/coder/project" \
  -v ${PWD}/ide.yaml:/home/coder/.config/code-server/config.yaml \
  -u "$(id -u):$(id -g)" \
  codercom/code-server:latest
